﻿# JGFMD (Just Go Mult Downloader)

---

 JGFMD是一个多线程下载器，与目前网络上的多线程下载器不同的地方是，它是已流的形似下载文件的。这样带来的好处就是能用最快的速度下载文件，还能像单线程下载的那样，能边下边播。
 


----------


###实现过程_V1
> * 1.通过JSON文件添加下载信息
> * 2.通过HEAD参数获取头文件信息
> * 3.使用`DownloadInfo.TestSpeed()`方法测试下载速度
> * 4.根据获取的下载速度调用`DownloadInfo.SetChunk(int64)`方法分块
> * 5.接着使用`DownloadInfo.SetupFile(string)`设置文件名称和创建文件 *注意：如果HEAD获取到的文件名称存在FileName参数里*
> * 6.最后用`DownloadInfo.Download(func)`方法即可开始下载


----------
###TODO_V0
- [X] 使用json添加下载
- [X] 获取HEAD信息并解析内容
- [X] 支持添加HEAD信息来请求
- [X] 下载速度测试
- [X] 根据下载速度分块
- [X] 使用git工具管理

----------


### TODO_V1
- [X] 下载超时(即速度慢),自动重试
- [X] 添加暂停功能
- [X] 初步修复文件下载异常
- [X] 深度修复、测试下载异常现象
- [X] 删除大部分调试的Log
- [ ] 实现下载速度统计
- [X] 自动的文件名处理，包括HEAD里的和URL信息里的
- [ ] 下载完成后，进行文件校验
- [X] 下载过程中的错误，自动处理，并输出到log里。*(初步)*
- [ ] 发现无法处理的致命性错误，**暂停**并反馈给用户
- [X] 请求信息时的User-Agent更改
- [ ] 改为一个任务一个进程
- [X] 修改包下载错误即重新下载全包的问题


----------

### TODO_V2
- [ ] 限速功能
- [ ] 网络通信接口-添加下载、暂停等操作
- [X] 优化内存
- [ ] 适配各个平台操作系统
- [ ] 使用GUI、WEB来管理
- [ ] 使用FFMPEG完成一个适合该软件的播放器