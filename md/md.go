package md

import (
	"config"
	"errors"
	//"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	//"sync"
	"runtime"
	"time"
)

const (
	fileWriteChunk = 1024 * 1024
)

var police int = 0
var speedAll int64 = 0
var alldown int64 = 0
var Xspeed float64
var con config.FormFileConfig //储存json的操作句柄

type DownloadInfo struct {
	Url         string            //下载地址
	Head        map[string]string //头信息
	Md5         string            //头信息里的md5
	Etag        string            //头信息里的etag
	FileName    string            //文件名称，从网页里获取的
	Size        int64             //文件大小
	AlreadySize int64             //已经下载的大小
	Ranges      bool              //是否可Ranges
	TheadNum    int               //线程数
	Chunks      []Chunk           //分块大小
	downstatus  *chan int         //状态频道
	saveAddress string            //保存地址
}

type Chunk struct {
	ID    int64
	Begin int64
	End   int64
	IsOK  bool
}

type Feedback struct {
	Chunk Chunk
}

func MdInit(file string) (*DownloadInfo, error) {
	if file != "" {
		f, err := os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			log.Println("error opening file: %v", err)
		}

		log.SetOutput(f)
		con = config.Finit()
		err = con.LoadConfigFormFile(file)
		if err != nil {
			log.Println("Mdint() Err:", err)
			return &DownloadInfo{}, err
		}

		header := make(map[string]string)
		a, err := con.Jsonhandle.GetPath("DownloadInformation", "Header").Map()
		if err != nil {
			log.Println("Json get Header err:", err)
			return &DownloadInfo{}, err
		}

		for k, v := range a {
			header[k] = v.(string) //Head
		}

		Thread_json, err := con.Jsonhandle.GetPath("DownloadInformation", "Thread").Int() //TheadNum
		if err != nil {
			log.Println("Json get Thread err:", err)
			return &DownloadInfo{}, err
		}

		Xspeed, err = con.Jsonhandle.GetPath("DownloadInformation", "Xspeed").Float64() //TheadNum
		if err != nil {
			log.Println("Json get Xspeed err:", err)
			Xspeed = float64(1)
			//return &DownloadInfo{}
		}

		u, err := con.Jsonhandle.GetPath("DownloadInformation", "URL").String() //Url
		if err != nil {
			log.Println("Json get URL err:", err)
			return &DownloadInfo{}, err
		}
		fileaddr, err := con.Jsonhandle.GetPath("DownloadInformation", "SaveAddr").String() //获取保存的目录
		if err != nil {
			log.Println("Json get URL err:", err)
			fileaddr = ""
		}
		con.IsContinue = con.ContinueOrNew()
		if con.IsContinue == true { //判断是否为继续下载的文件
			//Continue Download

			size, err := con.Jsonhandle.GetPath("ProgressRate", "AlreadyDownloaded").Int64() //AlreadySize
			if err != nil {
				log.Println("Json get AlreadyDownloaded err:", err)
				return &DownloadInfo{}, err
			}

			originalSize, err := con.Jsonhandle.GetPath("FileInformation", "Size").Int64() //Size
			if err != nil {
				log.Println("Json get Thread err:", err)
				return &DownloadInfo{}, err
			}

			subMd5, err := con.Jsonhandle.GetPath("DownloadInformation", "Md5").String() //判断是否有Md5

			if err != nil || subMd5 == "" {

			}

			SubRange, err := con.Jsonhandle.GetPath("DownloadInformation", "Ranges").Int() //是否支持断点续传
			var BoolRange bool
			if err != nil || SubRange == 1 {
				BoolRange = true
			} else {
				BoolRange = false
			}

			return &DownloadInfo{
				Url:         u,
				Head:        header,
				Size:        originalSize,
				AlreadySize: size,
				TheadNum:    Thread_json,
				Ranges:      BoolRange,
				Md5:         subMd5,
				saveAddress: fileaddr,
			}, nil
		} else {
			//Is new donload
			return &DownloadInfo{
				Url:         u,
				Head:        header,
				Size:        0,
				AlreadySize: 0,
				TheadNum:    Thread_json,
				saveAddress: fileaddr,
			}, nil
		}

	} else {
		return &DownloadInfo{}, errors.New("文件名为空")
	}
	return &DownloadInfo{}, errors.New("未知错误")
}
func (di *DownloadInfo) GetherInfo() error {

	//<<获取HTTP头信息>>
	var client = http.Client{}
	req, err := http.NewRequest("HEAD", di.Url, nil)
	if err != nil {
		return err
	}
	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		if len(di.Head) > 0 {
			for key, context := range di.Head {
				req.Header.Add(key, context)
			}
		}
		return nil
	}

	if len(di.Head) > 0 {
		for Headerkey, Headercontext := range di.Head {
			req.Header.Set(Headerkey, Headercontext)
		}
	}

	res, err := client.Do(req)

	if err != nil {
		log.Println("Client.Do ERR:", err)
	}
	//<<获取HTTP头结束>>
	ifClientError := res.StatusCode - 400 //4XX的错误-400会大于0
	ifServerError := res.StatusCode - 500 //5XX的错误-500会大于0

	if ifClientError > 0 || ifServerError > 0 {
		sc := strconv.Itoa(res.StatusCode)
		return errors.New("Status Code" + sc)
	}

	ContentLength := res.Header.Get("Content-Length")
	if ContentLength == "" {
		return errors.New("Content-Length ERR")
	}
	di.Size, err = strconv.ParseInt(ContentLength, 0, 64)
	if err != nil {
		log.Println("strconv.ParseInt Err:", err)
	}

	if res.Header.Get("Accept-Ranges") != "" {
		di.Ranges = true
	} else {
		di.Ranges = false
	}

	di.Etag = res.Header.Get("Etag")
	di.Md5 = res.Header.Get("Content-MD5")

	f_name_tmp := res.Header.Get("Content-Disposition")
	if len(f_name_tmp) > 0 {
		di.FileName = strings.Trim(Between(f_name_tmp, "filename=", ""), "\"")
	} else {
		filename1 := filepath.Base(di.Url)

		di.FileName = filename1
	}
	defer res.Body.Close()
	return nil
}
func Between(str, starting, ending string) string { //打酱油的文字处理函数
	var s, e int
	if starting == "" && ending == "" {
		return ""
	}

	if starting == "" {
		s = 0
		e = strings.Index(str, ending)
		return str[0:e]
	} else if ending == "" {
		e = 0
		s = strings.Index(str, starting)
		s += len(starting)
		return str[s:len(str)]
	} else {
		s = strings.Index(str, starting)
		if s < 0 {
			return ""
		}
		s += len(starting)
		e = strings.Index(str[s:], ending)
		if e < 0 {
			return ""
		}
		return str[s : s+e]

	}
}
func MakeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}
func (di *DownloadInfo) SetupFile(newfilename string) error {
	if len(newfilename) <= 0 {
		return errors.New("File address is empty")
	}
	di.saveAddress = di.saveAddress + newfilename
	if _, err := os.Stat(di.saveAddress); os.IsNotExist(err) {
		//文件不存在
		file, err := os.Create(di.saveAddress)
		if err != nil {
			return err
		}
		err = file.Truncate(di.Size)
		defer file.Close()

		if err != nil {
			return err
		}

	} else {
		//文件存在
		return nil
	}
	return nil
}
func (di *DownloadInfo) TestSpeed() (int64, error) {

	url_address := di.Url
	var client = http.Client{
		Transport: &http.Transport{
			Dial: func(netw, addr string) (net.Conn, error) {
				c, err := net.DialTimeout(netw, addr, time.Second*3) //设置建立连接超时
				if err != nil {
					return nil, err
				}
				c.SetDeadline(time.Now().Add(5 * time.Second)) //设置发送接收数据超时
				return c, nil
			},
		},
	}
	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		if len(di.Head) > 0 {
			for key, context := range di.Head {
				req.Header.Add(key, context)
			}
		}
		url_a_temp := req.URL
		url_address = url_a_temp.String()

		return nil
	}
	req, _ := http.NewRequest("GET", url_address, nil)
	if len(di.Head) > 0 {
		for key, context := range di.Head {
			req.Header.Add(key, context)
		}
	}
	var Data int64

	if di.Size > int64(1048576) {
		req.Header.Add("Range", "bytes=0-1048570")
		Data = int64(1048576)
	} else if di.Size > int64(524288) {
		req.Header.Add("Range", "bytes=0-524288")
		Data = int64(524288)
	} else {
		return 0, errors.New("不需要测试")
	}

	UnixTime_A := MakeTimestamp()

	res, err := client.Do(req)
	if err != nil {
		if neterr, ok := err.(net.Error); ok && neterr.Timeout() {

		} else {
			log.Println("SpeedError:", err)
			err.Error()
			return 0, err
		}

	}
	var speed int64 = 0
	log.Print("开始测试")

	content, err := ioutil.ReadAll(res.Body)
	if err != nil {

		if neterr, ok := err.(net.Error); ok && neterr.Timeout() {

		} else {
			log.Println(err)
		}
	}
	if len(content) >= 102400 {
		UnixTime_B := MakeTimestamp()
		s := UnixTime_B - UnixTime_A
		speed = int64(((float32(Data) / float32(s/1000)) / 1024))
		log.Println(Data, "数据下载成功,用了", s, "毫秒秒,相当于", speed, "KB/s")
	}

	return speed, nil
}

func (di *DownloadInfo) GetOldChunk() error {
	if di.AlreadySize > 0 {
		IsEmptyProgressRate, err := con.Jsonhandle.GetPath("ProgressRate", "Package").Array()
		context, err := config.ToArrInt64(IsEmptyProgressRate)
		if err != nil {
			return err
		}
		for p := 0; p < len(*context); p++ {
			di.Chunks = append(di.Chunks, Chunk{
				ID:    int64(p),
				Begin: (*context)[p][0],
				End:   (*context)[p][1],
				IsOK:  false,
			})
		}
		log.Println("目前di.Chuck大小为", len(di.Chunks))

		return nil
	} else {
		return errors.New("AlreadySize<=0")
	}
}

func (di *DownloadInfo) SetChunk(speed int64) error {
	if di.Ranges == false {
		di.Chunks = append(di.Chunks, Chunk{
			ID:    0,
			Begin: 0,
			End:   di.Size,
			IsOK:  false,
		})
	}
	if di.Size > 10*1024*1024 { // 文件大与10M
		if speed > 0 {
			n := int64(float64(speed*1024) * Xspeed) //分块大小
			log.Println("399 L ", n)
			leftover := di.Size % n
			var i int64 = 0
			var id int64 = 0
			for i < di.Size {
				if i == (di.Size - leftover) {
					di.Chunks = append(di.Chunks, Chunk{
						ID:    id,
						Begin: i,
						End:   i + leftover,
						IsOK:  false,
					})
					break
				}
				if i < (di.Size - leftover) {
					di.Chunks = append(di.Chunks, Chunk{
						ID:    id,
						Begin: i,
						End:   i + n,
						IsOK:  false,
					})

					i = i + n
				}
				id = id + 1
			}
			return nil
		} else {
			return errors.New("Speed 为0")
		}
	} else if di.Size <= 10*1024*1024 && di.Size > 0 {
		di.Chunks = append(di.Chunks, Chunk{
			ID:    0,
			Begin: 0,
			End:   di.Size,
			IsOK:  false,
		})
		return nil
	} else {
		return errors.New("文件大小为0，可能的原因:网络资源拉取错误")
	}
}

func (di *DownloadInfo) Download(fb func(Feedback)) {
	if di.Ranges == true && di.TheadNum > 1 {
		status := make(chan int) //stop:0

		di.downstatus = &status
		downChan := make(chan Chunk)
		downMess := make(chan Chunk)
		endRun := false

		file, err := os.OpenFile(di.saveAddress, os.O_WRONLY, 0666)
		if err != nil {
			log.Println("Download 文件无法打开", di.saveAddress, err)
			return
		}
		goroutines := func(downChan *chan Chunk, downMess *chan Chunk, file *os.File, bufgo *[]byte) {

			for {
				select {

				case dc := <-*downChan: // 收到下载信号

				TOAGAIN:

					err := di.mutidownload(file, dc, bufgo) //直接扔给多线程下载函数
					if err != nil {
						log.Println(err)
						goto TOAGAIN
					}
					dc.IsOK = true
					*downMess <- dc

					break

				}
			}
			runtime.GC()

		}
		var id int64 = 0
		var dowList int
		if len(di.Chunks) < di.TheadNum {
			log.Println("分块有问题,块数小于线程数，已经自动调整")
			di.TheadNum = len(di.Chunks)

		}
		if len(di.Chunks) < 1 {
			log.Println("CHUCKS太小了")
			return
		}
		for i := 0; i < di.TheadNum; i++ {
			bufSize := di.Chunks[0].End - di.Chunks[0].Begin
			buf := make([]byte, bufSize+1)
			go goroutines(&downChan, &downMess, file, &buf)
			downChan <- di.Chunks[id]
			id = id + 1
			dowList += 1
		}
		var idList []int64
		var stoplist [][]int64
	Run:
		for endRun == false {
			select {

			case message := <-downMess:
				if message.IsOK == false {
					//fb(Feedback{message})
					log.Println("ID:", message.ID, " 下载失败，块：", message.Begin, message.End)
					downChan <- message
				} else {
					dowList -= 1
					fb(Feedback{message})
					idList = append(idList, message.ID)
					if int64(len(idList)) < int64(len(di.Chunks)) {

						if int64(len(idList))+int64(dowList) != int64(len(di.Chunks)) {
							downChan <- di.Chunks[id]
							dowList += 1
							id = id + 1
							runtime.GC()
						}

					} else if int64(len(idList)) == int64(len(di.Chunks)) && dowList == 0 && police == len(idList) {
						runtime.GC()
						//全部完成

						file.Close()
						endRun = true
						fb(Feedback{
							Chunk{
								ID:    0,
								Begin: 0,
								End:   0,
								IsOK:  true,
							},
						})
						goto Run
					}

				}
				break

			case s := <-(*di.downstatus):

				if s == 0 {
					log.Println("downstatus = 0")
					stoplist = di.stopList(&idList)
					endRun = true
					goto Run
				}
				break
			}
		}

		if len(stoplist) != 0 {
			//说明目前已经暂停，且需要写入数据到文件
			log.Println("StopToFile")
			di.stopTofile(stoplist)

			os.Exit(1)
		}
	}
	if di.Ranges == false {
		//说明不支持断点续传
		//开始单线程下载
		okm, err := di.singleHttpDownload()
		if err != nil {
			log.Println("568L ", err)
			fb(Feedback{
				Chunk{
					ID:    -1,
					Begin: -1,
					End:   -1,
					IsOK:  okm,
				},
			})
		}
		log.Println("当前是单线程下载")
	} else if di.Ranges == true && di.TheadNum <= 0 {
		//说明线程未设置
		log.Println("di.TheadNum > 0")
		fb(Feedback{
			Chunk{
				ID:    -1,
				Begin: -1,
				End:   -1,
				IsOK:  false,
			},
		})

	}

}
func (di *DownloadInfo) stopTofile(stoplist [][]int64) {
	con.Jsonhandle.SetPath([]string{"FileInformation", "FileName"}, di.FileName)
	con.Jsonhandle.SetPath([]string{"FileInformation", "Size"}, di.Size)
	con.Jsonhandle.SetPath([]string{"ProgressRate", "Package"}, stoplist)
	con.Jsonhandle.SetPath([]string{"ProgressRate", "AlreadyDownloaded"}, alldown)
	if di.Ranges {
		con.Jsonhandle.SetPath([]string{"DownloadInformation", "Ranges"}, 1)
	} else {
		con.Jsonhandle.SetPath([]string{"DownloadInformation", "Ranges"}, 0)
	}

	if di.Md5 != "" {
		con.Jsonhandle.SetPath([]string{"DownloadInformation", "Md5"}, di.Md5)
	}

	cv, err := con.Jsonhandle.MarshalJSON() //合成json
	if len(cv) > 0 {
		con.SaveJson(cv)
		return
	} else {
		log.Println(err)
	}
}

func (di *DownloadInfo) mutidownload(file *os.File, c Chunk, buf *[]byte) error {
	url_address := di.Url
	redirect := func(req *http.Request, via []*http.Request) error {
		url_address = req.URL.String()
		di.Url = url_address
		return errors.New("UrlBad")
	}
	var client = http.Client{
		CheckRedirect: redirect,
		Transport: &http.Transport{
			Dial: func(netw, addr string) (net.Conn, error) {
				c, err := net.DialTimeout(netw, addr, time.Second*3) //设置建立连接超时
				if err != nil {
					return c, err
				}
				ti := 15
				if speedAll != 0 {
					ti = int((di.Chunks[0].End-di.Chunks[0].Begin)/(speedAll*1024)) + 1
				}

				c.SetDeadline(time.Now().Add(time.Duration(ti*5) * time.Second)) //设置发送接收数据超时
				return c, nil
			},
		},
	}

XX:
	req, err := http.NewRequest("GET", url_address, nil)
	req.Close = true
	if len(di.Head) > 0 {
		for key, context := range di.Head {
			req.Header.Add(key, context)
		}
	}
	req.Header.Add("Range", "bytes="+strconv.FormatInt(c.Begin, 10)+"-"+strconv.FormatInt(c.End, 10))

	res, err := client.Do(req)
	if err != nil {

		if neterr, ok := err.(net.Error); ok && neterr.Timeout() {

			return errors.New("超时了")
		} else {
			res.Body.Close()
			goto XX
			return err
		}
	}

	if int64(len(*buf)) <= (c.End - c.Begin) {
		res.Body.Close()
		return errors.New("BUF太小")
	}
	nREAD, err := io.ReadFull(res.Body, *buf)
	if err != nil {

		if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
			res.Body.Close()
			return errors.New("超时了")
		}
		if err == io.EOF {
			res.Body.Close()
			return nil
		}
	}
	n, err := file.WriteAt((*buf)[:nREAD], c.Begin)
	if err != nil {
		log.Println("写入错误", err)
	}
	if int64(n) < (c.End - c.Begin) {
		res.Body.Close()
		c.Begin += int64(n)
		log.Println(c.ID, "写入的太小，打回重下", "Raw:", c.End-c.Begin, " Now:", n, " And now change ", c.Begin, " to ", c.End)
		return errors.New("few")
	}
	police = police + 1
	res.Body.Close()
	runtime.GC()

	return nil
}

func (di *DownloadInfo) singleHttpDownload() (IsOK bool, err error) {

	addr := di.saveAddress
	var client = http.Client{}
	var url_address string
	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {

		url_a_temp := req.URL
		url_address = url_a_temp.String()

		return nil
	}
	req, err := http.NewRequest("GET", url_address, nil)

	if len(di.Head) > 0 {
		for key, context := range di.Head {
			req.Header.Add(key, context)
		}
	}
	res, err := client.Do(req)
	if err != nil {
		return false, err
	}
	defer res.Body.Close()
	file, err := os.OpenFile(addr, os.O_WRONLY, 0666)
	defer file.Close()
	if err != nil {
		return false, err
	}
	_, err = copyBuffer(file, res.Body, nil, int64(0))

	if err != nil {
		res.Close = true
		return false, err
	}
	res.Close = true
	return true, nil
}
func copyBuffer(dst io.Writer, src io.Reader, buf []byte, ATpoint int64) (written int64, err error) { // This function from Internet,Thanks
	// If the reader has a WriteTo method, use it to do the copy.
	// Avoids an allocation and a copy.
	if wt, ok := src.(io.WriterTo); ok {
		return wt.WriteTo(dst)
	}
	// Similarly, if the writer has a ReadFrom method, use it to do the copy.
	if rt, ok := dst.(io.ReaderFrom); ok {
		return rt.ReadFrom(src)
	}
	if buf == nil {
		buf = make([]byte, 1024*1024)
	}
	for {
		nr, er := src.Read(buf)
		if nr > 0 {
			nw, ew := dst.Write(buf[ATpoint:nr])
			if nw > 0 {
				written += int64(nw)
			}
			if ew != nil {
				err = ew
				break
			}
			if nr != nw {
				err = io.ErrShortWrite
				break
			}
		}
		if er == io.EOF {
			break
		}
		if er != nil {
			err = er
			break
		}
	}
	return written, err
}

func (di *DownloadInfo) DownloadStatus(i int) {
	(*di.downstatus) <- i
}

func (di *DownloadInfo) stopList(idList *[]int64) [][]int64 {
	var stop [][]int64
	log.Println("开始取出stoplist")
	for i := 0; i < len(di.Chunks); i++ {
		bool_val, int_val := searchArray(idList, di.Chunks[i].ID)
		if bool_val == true && int_val > -1 {
			//能找到就表示已经下个过了，直接忽略
			alldown += di.Chunks[i].End - di.Chunks[i].Begin
			continue
		} else {
			//把未找到的，即未下载的放到一个数组里，传回去
			stop = append(stop, []int64{di.Chunks[i].Begin, di.Chunks[i].End})
		}
	}
	log.Println("好了，目前", len(stop))
	return stop
}

func searchArray(data *[]int64, searchData int64) (bool, int) {
	if len(*data) == 0 {
		return false, -1
	}
	for i := 0; i < len(*data); i++ {
		if (*data)[i] == searchData {
			return true, i
		}
	}
	return false, -1
}
