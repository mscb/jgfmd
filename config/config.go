package config

import (
	"errors"

	"os"

	json1 "github.com/bitly/go-simplejson"
	//"reflect"
	"encoding/json"
)

/*
func main(){

    var a FormFileConfig
    fmt.Println(a.LoadConfigFormFile("newjson.json"),a.JsonFile)

    //fmt.Println("Continue Or New ",a.ContinueOrNew(),a.Jsonhandle)
    IsEmptyProgressRate,err := a.Jsonhandle.Get("ProgressRate").Get("Package").Array()


    context ,err := ToArrInt64(IsEmptyProgressRate)
    fmt.Println((*context)[0],err)
    (*context)[0] = []int64{1,1}
    var aa interface{}
    aa = (*context)
    a.Jsonhandle.SetPath([]string{"ProgressRate","Package"},aa)
    cv,err := a.Jsonhandle.MarshalJSON()
    if len(cv) > 0{
        a.SaveJson(cv)
    }else{
        fmt.Print("!!")
    }

}*/
type FormFileConfig struct {
	JsonFile   string
	IsContinue bool
	Jsonhandle *json1.Json
}

func Finit() (a FormFileConfig) {
	return a
}

func (f *FormFileConfig) LoadConfigFormInput(body []byte) (err error) {
	f.Jsonhandle, err = json1.NewJson(body)
	if err != nil {
		return err
	}
	f.JsonFile = "NewConfig.json"
	return nil
}

func (f *FormFileConfig) LoadConfigFormFile(fileName string) error {
	file, err := os.OpenFile(fileName, os.O_RDWR, 0666)
	if err != nil {

		return err
	}
	f.JsonFile = fileName
	f.Jsonhandle, err = json1.NewFromReader(file)
	if err != nil {

		return err
	}
	defer file.Close()
	return nil
}

func (f *FormFileConfig) ContinueOrNew() bool { //Ture is Continue, False is New
	size_tmp, _ := f.Jsonhandle.GetPath("FileInformation", "Size").Int64()

	if size_tmp <= 0 {
		return false
	}
	return true

}

func ToArrInt64(PackageInterface []interface{}) (*[][]int64, error) {
	var part [][]int64
	if len(PackageInterface) > 0 {

		for i := 0; i < len(PackageInterface); i++ {

			s := PackageInterface[i].([]interface{})
			ss1, err := (s[0].(json.Number)).Int64()
			ss2, err := (s[1].(json.Number)).Int64()
			if len(s) > 0 {
				part = append(part, []int64{ss1, ss2})
			}
			if err != nil {
				return nil, nil
			}
		}

	} else {
		return nil, errors.New("<0")
	}
	return &part, nil
}

func (f *FormFileConfig) SaveJson(p []byte) error {
	file, err := os.OpenFile(f.JsonFile, os.O_TRUNC|os.O_WRONLY, 0666)
	if err != nil {
		return err
	}
	_, err = file.Write(p)
	if err != nil {
		return err
	}
	defer file.Close()
	return nil
}
