package main

import (
	"fmt"
	"md"
	"os"
	"runtime"
)

var nowdownsize int64 = 0
var osize int64 = 0

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	com, err := md.MdInit("json.json")

	var input string

	if err != nil {
		fmt.Println("17L ", err)
		fmt.Scanf("%s\n", &input)
	}

	if com.AlreadySize <= 0 {
		fmt.Println("开始新的下载进程")
		err := com.GetherInfo()
		if err != nil {
			fmt.Print(err)
		}
		c, err := com.TestSpeed()
		if err != nil || c <= 0 {
			fmt.Print(err, c)
		}

		err = com.SetChunk(c * 5)
		if len(com.Chunks) <= 0 {
			fmt.Print("分包错误")
			fmt.Scanf("%s\n", &input)
		}

		everChuck := int64((com.Chunks[0].End - com.Chunks[0].Begin) / int64(1024))

		fmt.Println("分了", len(com.Chunks), "个包,每个包大小为", everChuck, "Kb")
		if err != nil {
			fmt.Print(err)
		}
	} else {
		err := com.GetherInfo()
		if err != nil {
			fmt.Print(err)
		}
		fmt.Println("继续下载")
		com.GetOldChunk()

	}
	osize = com.Size
	nowdownsize = com.AlreadySize

	var name string = "data.new"
	if len(com.FileName) > 0 {
		name = com.FileName
	}
	err = com.SetupFile(name)
	if err != nil {
		fmt.Print(err)
	}

	fmt.Println("一共分了 ", len(com.Chunks), " 个包")
	go com.Download(fb)

	fmt.Printf("\rComplied:%.2f%%  #", float32(float32(nowdownsize)/float32(osize)*100))

	fmt.Scanf("%s\n", &input)
	if input == "stop" {
		fmt.Println("stop!")
		com.DownloadStatus(0)
	}
	select {}

}
func fb(a md.Feedback) {
	if a.Chunk.ID == -1 {
		fmt.Println("下载出现错误！")
	}
	if a.Chunk.IsOK == true {
		nowdownsize += a.Chunk.End - a.Chunk.Begin
	}

	fmt.Printf("\rComplied:%.2f%%  #", float32(float32(nowdownsize)/float32(osize)*100))

	if a.Chunk.Begin == 0 && a.Chunk.End == 0 {
		fmt.Printf("\r全部完成           ")
		os.Exit(1)
	}

}
